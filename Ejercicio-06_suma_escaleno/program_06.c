/* Programa para calcular el perimetro de un triangulo cualquiera*/

#include<conio.h>
#include<stdio.h>

int main()

{
	int a;
	int b;
	int c;
	int perimetro;

	printf("\n Introduzca el primer lado del triangulo: ");
	scanf("%d", &a);

	printf("\n Introduzca el segundo lado del triangulo: ");
	scanf("%d", &b);

	printf("\n Introduzca el tercer lado del triangulo: ");
	scanf("%d", &c);

	perimetro = a + b + c;
	printf("\n El perimetro del triangulo es el siguiente: %d", perimetro);

	getch();

	return 0;
}
