# Ejercicios del curso Introducción a la programación

En este repositorio se encuentran los programas realizados sobre problemas que han asignado a lo largo del curso de Introducción a la Programación.

## Descripción

En este curso se abordan los conceptos básicos y las buenas prácticas para una introducción correcta a la programación con sus respectivos Pseudocódigos y Diagramas de Actividad en UML, Procedimiento y Código en C es opcional.

## Autor

Barajas Cervantes Alfonso

### Contacto

albace2000@hotmail.com
