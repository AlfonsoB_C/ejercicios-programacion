/* Programa que calcula el perimetro de un Tri'angulo Equil'atero */

#include<conio.h>
#include<stdio.h>

int main()
{
	float lado;
	float perimetro;

	printf("\n	Introduzca el lado del triangulo equilatero: ");
	scanf("%f", &lado);

	perimetro = 3 * lado;

	printf("\n	El perimetro del triangulo equilatero es:  %f", perimetro);

	getch();

	return 0;

}
