/* Programa que calcula el perimetro de un triangulo Isosceles*/
#include<conio.h>
#include<stdio.h>

int main()

{
	int lado_desigual;
	int lado_igual;
	int perimetro;

	printf("\n	Introduzca el lado que es igual en el triangulo isosceles: ");
	scanf("%d", &lado_igual);
	printf("\n	Ahora, introduce el lado que es diferente a los demas: ");
	scanf("%d", &lado_desigual);
	perimetro = 2*lado_igual + lado_desigual;
	printf("\n	El perimetro del Triangulo Isosceles es: %d ", perimetro);

	getch();

	return 0;

}
