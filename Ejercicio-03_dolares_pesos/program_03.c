/* Programa de Dolares a Pesos Mexicanos*/

#include<conio.h>
#include<stdio.h>

#define MX 21.98
int main()
{
	float dolares;
	float pesosmexicanos;

	printf("\n	Introduzca cantidad de dolares: ");
	scanf( "%f", &dolares);

	pesosmexicanos = dolares * MX;

	printf("\n	Equivalen a: %f pesos mexicanos", pesosmexicanos);

	getch();

	return 0;

}
